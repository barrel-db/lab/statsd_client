%% Copyright 2017, Bernard Notarianni
%% Copyright 2017, Benoit Chesneau
%%
%% This file is part of barrel_metrics released under the BSD license.
%% See the NOTICE for more information.
%%
-module(statsd_client).

-author("Bernard Notarianni").

-behaviour(gen_server).

%% plugin callbacks
-export([ new/2
        , increment/2
        , set_value/2
        , duration/2
        ]).

%% gen_server callbacks

-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2]).
-export([handle_info/2, terminate/2, code_change/3]).


-record(state, {socket, key}).

%% Specification for statsd message format:
%% https://github.com/etsy/statsd/blob/master/docs/metric_types.md

new(_Type, _Name) ->
  ok.

set_value(Name, Value) ->
  push(Name, {gauge, Value}),
  ok.

increment(Name, Value) ->
  push(Name, {counter, Value}),
  ok.

duration(Name, Value) ->
  push(Name, {duration, Value}),
  ok.

push(MetricName, Value) ->
  gen_server:cast(?MODULE, {send, MetricName, Value}).


%% gen_server api

start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%%% Server functions
init(_) ->
  Server = application:get_env(statsd_client, server, undefined),
  init_server(Server).

init_server(undefined) ->
  {ok, #state{}};
init_server({Peer, Port}) ->
  [Node, Host] = binary:split(atom_to_binary(node(), utf8), <<"@">>),
  HostWithoutDots = binary_join(binary:split(Host, <<".">>, [global]), <<"_">>),
  StatsdKey = binary_join([HostWithoutDots, Node], <<".">>),
  {ok, Socket} = gen_udp:open(0),
  _ = lager:info("init statsd metrics peer=~p port=~p", [Peer, Port]),
  {ok, #state{socket={Socket, Peer, Port}, key=StatsdKey}}.

handle_call(terminate, _From, State) ->
  {stop, normal, ok, State}.

handle_cast({send, _MetricName, _Value}, #state{socket=undefined}=State) ->
  {noreply, State};
handle_cast({send, MetricName, Value}, #state{socket=Socket, key=ServerKey}=State) ->
  MetricJoined = binary_join(MetricName, <<".">>),
  FullKey = binary_join([ServerKey, MetricJoined], <<".">>),
  send(Socket, FullKey, Value),
  {noreply, State}.

handle_info(_, State) ->
  {noreply, State}.

terminate(normal, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

send(Socket, Key, {counter, Value}) ->
  BVal = integer_to_binary(Value),
  Data = <<Key/binary, ":", BVal/binary, "|c">>,
  udp(Socket, Data);

send(Socket, Key, {gauge, Value}) ->
  BVal = integer_to_binary(Value),
  Data = <<Key/binary, ":", BVal/binary, "|g">>,
  udp(Socket, Data);

send(Socket, Key, {duration, Value}) ->
  BVal = integer_to_binary(Value),
  Data = <<Key/binary, ":", BVal/binary, "|ms">>,
  udp(Socket, Data).

udp({Socket, Peer, Port}, Data) ->
  _ = gen_udp:send(Socket, Peer, Port, Data),
  ok.



binary_join([], _Sep) -> <<>>;
binary_join([Part], _Sep) -> to_binary(Part);
binary_join([Head|Tail], Sep) ->
  lists:foldl(
    fun (Value, Acc) -> <<Acc/binary, Sep/binary, (to_binary(Value))/binary>> end,
    to_binary(Head),
    Tail
   ).

to_binary(V) when is_binary(V) -> V;
to_binary(V) when is_list(V) -> list_to_binary(V);
to_binary(V) when is_atom(V) -> atom_to_binary(V, utf8);
to_binary(V) when is_integer(V) -> integer_to_binary(V);
to_binary(_) -> error(badarg).
