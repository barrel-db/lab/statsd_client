%% Copyright 2017, Bernard Notarianni
%%
%% This file is part of barrel_metrics released under the BSD license.
%% See the NOTICE for more information.
%%

%%%-------------------------------------------------------------------
%% @doc statsd_client top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(statsd_client_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

init([]) ->
  Statsd =
    #{id => barrel_statsd,
      start => {statsd_client, start_link, []},
      restart => permanent,
      shutdown => 2000,
      type => worker,
      modules => [statsd_client]},

  {ok, { {one_for_all, 10000, 1}, [Statsd]} }.
