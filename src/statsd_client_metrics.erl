%% Copyright 2017, Bernard Notarianni
%% Copyright 2017, Benoit Chesneau
%%
%% This file is part of barrel_metrics released under the BSD license.
%% See the NOTICE for more information.
%%
%% @doc erlang-metrics backend
%%
-module(statsd_client_metrics).

-export([new/3, update/3, update_or_create/3, delete/2]).


new(counter, _Name, _Config) -> ok;
new(gauge, _Name, _Config) -> ok;
new(duration, _Name, _Config) -> ok;
new(_Type, _Name, _Config) -> {error, unsupported_type}.


update(Name, {c, I}, _Config) when I > 0 ->
  statsd_client:increment(Name, I);
update(Name, {c, I}, _Config) when I < 0 ->
  statsd_client:decrement(Name, I);
update(Name, timer_start, _Config) ->
  put({timer, Name}, erlang:timestamp());
update(Name, timer_end, _Config) ->
  EndTime = erlang:timestamp(),
  case erase({timer, Name}) of
    undefined -> ok;
    StartTime ->
      Time = timer:now_diff(EndTime, StartTime),
      statsd_client:duration(Name, trunc(Time / 1000))
  end;
update(Name, {duration_fun, Fun}, _Config) ->
  {Time, Value} = timer:tc(Fun),
  statsd_client:duration(Name, trunc(Time / 1000)),
  Value.

update_or_create(Name, Type, Config) -> update(Name, Type, Config).


delete(_Name, _Config) -> ok.



