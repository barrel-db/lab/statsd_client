%% Copyright 2017, Bernard Notarianni
%%
%% This file is part of barrel_metrics released under the BSD license.
%% See the NOTICE for more information.
%%

%%%-------------------------------------------------------------------
%% @doc statsd_client public API
%% @end
%%%-------------------------------------------------------------------

-module(statsd_client_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    statsd_client_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
